/* Author: Geanina Tambaliuc */
#include <stdio.h>
#include <stdlib.h>

//Struct to store student information
struct student {

    char name[50];
    int age;
    float GPA;
    char level[30];
};
int main()
{
    //Initializing the struct

    struct student s1= {"John Smith", 22, 3.98, "Senior"};
    struct student s2= {"Jane Doe", 19, 4.00, "Sophomore"};

    //Printing
    printf("Student 1 information: %s, %d, %f,  %s \n", s1.name, s1.age, s1.GPA, s1.level);
    printf("Student 2 information: %s, %d, %f,  %s \n", s2.name, s2.age, s2.GPA, s2.level);
    return 0;
}
